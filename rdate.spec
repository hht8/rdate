%global debug_package %{nil}
Summary: Tool for getting the date/time from a remote machine
Name: rdate
Version: 1.5
Release: 2
License: GPLv2+
Source: https://www.aelius.com/njh/rdate/%{name}-%{version}.tar.gz
URL: https://www.aelius.com/njh/rdate/
BuildRequires: gcc
%description
rdate connects to an RFC 868 time server over a TCP/IP network, printing the returned time and/or setting the system clock.

%package_help

%prep
%autosetup -n %{name}-%{version}

%build
%{configure} --prefix=%{buildroot}%{_bindir}
%{make_build} CFLAGS="-DINET6 -fno-strict-aliasing -fPIC"

%install
%{make_install}

%files
%doc COPYING
%attr(0755,root,root) %{_bindir}/rdate

%files help
%{_mandir}/man1/rdate.1*

%changelog
* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 1.5-2
- Completing build dependencies to fix gcc compiler missing error

* Mon Mar 15 2021 huanghaitao <huanghaitao8@huawei.com> - 1.5-1
- Add fPIC parameters to prevent symbol redirection failure

* Thu Nov 07 2019 openEuler Buildtam <buildteam@openeuler.org> - 1.5-0
- Package Init
